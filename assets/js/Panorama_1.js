/**
 * 說明：Panorama 全景
 * <script>
 * VR.init();
 * VR.animate();
 * </script>
 */
var camera, scene, renderer;
var isUserInteracting = false, // 自動旋轉
    onMouseDownMouseX = 0, onMouseDownMouseY = 0,
    lon = 0, onMouseDownLon = 0,
    lat = 0, onMouseDownLat = 0,
    phi = 0, theta = 0;

var Panorama_1 = {
    init: function () {
        var container, mesh;
        container = document.getElementById('container');

        camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 1, 1100);
        camera.target = new THREE.Vector3(0, 0, 0);

        scene = new THREE.Scene();

        var geometry = new THREE.SphereBufferGeometry(500, 60, 40);
        geometry.scale(-1, 1, 1);

        var texture = new THREE.TextureLoader().load('img/SphericalMap.jpg');
        var material = new THREE.MeshBasicMaterial({map: texture});

        mesh = new THREE.Mesh(geometry, material);

        scene.add(mesh);

        renderer = new THREE.WebGLRenderer();
        renderer.setPixelRatio(window.devicePixelRatio);
        renderer.setSize(window.innerWidth, window.innerHeight);
        container.appendChild(renderer.domElement);

        document.addEventListener('mousedown', this.onPointerStart, false);
        document.addEventListener('mousemove', this.onPointerMove, false);
        document.addEventListener('mouseup', this.onPointerUp, false);

        document.addEventListener('wheel', this.onDocumentMouseWheel, false);

        document.addEventListener('touchstart', this.onPointerStart, false);
        document.addEventListener('touchmove', this.onPointerMove, false);
        document.addEventListener('touchend', this.onPointerUp, false);

        document.addEventListener('dragover', function (event) {
            event.preventDefault();
            event.dataTransfer.dropEffect = 'copy';
        }, false);

        document.addEventListener('dragenter', function () {
            document.body.style.opacity = 0.5;
        }, false);

        document.addEventListener('dragleave', function () {
            document.body.style.opacity = 1;
        }, false);

        document.addEventListener('drop', function (event) {
            event.preventDefault();
            var reader = new FileReader();
            reader.addEventListener('load', function (event) {
                material.map.image.src = event.target.result;
                material.map.needsUpdate = true;
            }, false);
            reader.readAsDataURL(event.dataTransfer.files[0]);
            document.body.style.opacity = 1;
        }, false);

        window.addEventListener('resize', this.onWindowResize, false);
    },

    onWindowResize: function () {
        camera.aspect = window.innerWidth / window.innerHeight;
        camera.updateProjectionMatrix();

        renderer.setSize(window.innerWidth, window.innerHeight);
    },

    onPointerStart: function (event) {
        isUserInteracting = true;

        var clientX = event.clientX || event.touches[0].clientX;
        var clientY = event.clientY || event.touches[0].clientY;

        onMouseDownMouseX = clientX;
        onMouseDownMouseY = clientY;

        onMouseDownLon = lon;
        onMouseDownLat = lat;
    },

    onPointerMove: function (event) {
        if (isUserInteracting === true) {
            var clientX = event.clientX || event.touches[0].clientX;
            var clientY = event.clientY || event.touches[0].clientY;

            lon = (onMouseDownMouseX - clientX) * 0.1 + onMouseDownLon;
            lat = (clientY - onMouseDownMouseY) * 0.1 + onMouseDownLat;
        }
    },

    onPointerUp: function () {
        isUserInteracting = false;
    },

    /**
     * 說明：滑鼠滾輪事件
     * @param event
     */
    onDocumentMouseWheel: function (event) {
        var fov = camera.fov + event.deltaY * 0.05;
        camera.fov = THREE.Math.clamp(fov, 10, 75);
        camera.updateProjectionMatrix();
    },

    animate: function () {
        var animate = function (_this = this) {
            requestAnimationFrame(animate);
            Panorama_1.update();
        };
        animate();
    },

    update: function () {
        if (isUserInteracting === false) {
            // lon += 0.1;
        }

        lat = Math.max(-85, Math.min(85, lat));
        phi = THREE.Math.degToRad(90 - lat);
        theta = THREE.Math.degToRad(lon);

        camera.target.x = 500 * Math.sin(phi) * Math.cos(theta);
        camera.target.y = 500 * Math.cos(phi);
        camera.target.z = 500 * Math.sin(phi) * Math.sin(theta);

        camera.lookAt(camera.target);
        renderer.render(scene, camera);
    }
};
Panorama_1.init();
Panorama_1.animate();
