import * as THREE from '../plugins/threejs/build/three.module.js';
import {CSS2DObject} from "../plugins/threejs/jsm/renderers/CSS2DRenderer.js";
import {CSS2DRenderer} from "../plugins/threejs/jsm/renderers/CSS2DRenderer.js";
import {OrbitControls} from "../plugins/threejs/jsm/controls/OrbitControls.js";
import {TrackballControls} from "../plugins/threejs/examples/jsm/controls/TrackballControls.js";

'use strict';

var socket = io('https://info.ouorange.com:6001');
socket.on('connect', function () {
    socket.emit('chatroom', 'success');
});
var image;
var getCanvas;
setInterval(function () {
    socket.emit('chatroom', dataURIToBlob(image));

    // html2canvas($('#container'), {
    //     onrendered: function (canvas) {
            // var _canvas = document.getElementById('merge');
            // var context = _canvas.getContext('2d');
            // console.log(canvas.toDataURL('image/jpeg'));
            // context.drawImage(canvas);
            // context.drawImage(document.querySelector('#container canvas'), 0, 0, window.innerWidth, window.innerHeight);
            // var combined = new Image;
            // combined.src = _canvas.toDataURL('data/jpeg');
            // document.body.appendChild(combined);

            // socket.emit('chatroom', dataURIToBlob(document.getElementById('merge').toDataURL('image/jpeg')));
            // socket.emit('chatroom', dataURIToBlob(canvas.toDataURL('image/jpeg')));
    //     }
    // });
}, 300);

function dataURIToBlob(dataURI) {
    var byteString = atob(dataURI.split(',')[1]);
    var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0]
    var ab = new ArrayBuffer(byteString.length);
    var ia = new Uint8Array(ab);
    for (var i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
    }

    var blob = new Blob([ab], {type: mimeString});
    return blob;
}

var container;
var camera, camera1, camera2, scene, scene1, scene2, renderer, labelRenderer;
var current_cube; // 當前頁面的全景圖
var current_page;

var isUserInteracting = false, // 自動旋轉
    onMouseDownMouseX = 0, onMouseDownMouseY = 0,
    lon = 0, onMouseDownLon = 0,
    lat = 0, onMouseDownLat = 0,
    phi = 0, theta = 0;

var raycaster, mouse, points, controls;
var geometry;
var intersects;
var points_collect = [];

var distance = 400;
var dots;
var dot_intersects;
var transition_texture;
var clock = new THREE.Clock();

var getImageData = true;

var transition;
var transitionParams = {
    "useTexture": 0,
    "transition": 0.5,
    "transitionSpeed": 4.0,
    "texture": 5,
    "animateTransition": false
};

var scene_transition = false;
var time = 0;
var scene_interval;

var Panorama = {
    markers_collection: null,
    init: function () {
        this._ajax();
        raycaster = new THREE.Raycaster();
        mouse = new THREE.Vector2();

        container = document.getElementById('container');

        // camera = new THREE.PerspectiveCamera(80, window.innerWidth / window.innerHeight, 1, 1100);
        // camera.target = new THREE.Vector3(0, 0, 0);
        camera = new THREE.PerspectiveCamera(90, window.innerWidth / window.innerHeight, 0.1, 1000);
        camera.position.set(0, 0, 0.01);


        var path = 'img/cube/itour/';
        var format = '.jpg';
        var urls = [
            path + '1' + format, path + '3' + format,
            path + '4' + format, path + '5' + format,
            path + '0' + format, path + '2' + format
        ];

        // var path = 'img/cube/itour/';
        // var format = '.jpg';
        // var url2 = [
        //     path + '1' + format, path + '3' + format,
        //     path + '4' + format, path + '5' + format,
        //     path + '0' + format, path + '2' + format
        // ];


        var reflectionCube = new THREE.CubeTextureLoader().load(urls);
        transition_texture = new THREE.TextureLoader().load('img/transition/transition1.png');

        // var cube = new THREE.CubeTextureLoader().load(urls);
        // current_cube = cube;
        // scene1 = new FXScene(cube);
        // scene2 = new FXScene(new THREE.CubeTextureLoader().load(url2));
        //
        // camera1 = scene1.camera;
        // camera2 = scene2.camera;

        scene = new THREE.Scene();
        scene.background = reflectionCube;

        var helper = new THREE.GridHelper(1000, 40, 0x303030, 0x303030);
        // helper.position.y = -75;
        // scene.add(helper);
        // scene1.add(helper);

        // var helper = new THREE.GridHelper(1000, 40, 0x303030, 0x303030);
        // helper.position.y = -75;
        // scene2.add(helper);

        this.points();
        // this.labels();

        renderer = new THREE.WebGLRenderer({antialias: true, alpha: true, preserveDrawingBuffer: true});
        renderer.setPixelRatio(window.devicePixelRatio);
        renderer.setSize(window.innerWidth, window.innerHeight);
        container.appendChild(renderer.domElement);

        labelRenderer = new CSS2DRenderer();
        labelRenderer.setSize(window.innerWidth, window.innerHeight);
        labelRenderer.domElement.style.position = 'absolute';
        labelRenderer.domElement.style.top = 0;
        container.appendChild(labelRenderer.domElement);

        controls = new OrbitControls(camera, document.querySelector('#container'));
        controls.enablePan = false;
        controls.enableZoom = false;
        controls.enableDamping = true;
        controls.rotateSpeed = -0.25;
        controls.autoRotate = true;
        controls.autoRotateSpeed = 0.25;

        document.addEventListener('touchstart', this.onMouseDown, false);
        document.addEventListener('touchend', this.onMouseUp, false);
        document.addEventListener('touchmove', this.onMouseMove, false);

        document.addEventListener('mousedown', this.onMouseDown, false);
        // document.addEventListener('mousemove', this.onMouseMove, false);
        document.addEventListener('mouseup', this.onMouseUp, false);
        document.addEventListener('wheel', this.onDocumentMouseWheel, false);
        window.addEventListener('resize', this.onWindowResize, false);

        // transition = new Transition(scene1, scene2);
    },
    resetCameraPosition: function () {
        lon = 0;
        lat = 0;
        camera.position.set(0, 0, 0.01);
    },
    points: function () {
        this.markers_collection.data.markers.forEach(function (value) {
            var marker = document.createElement('div');
            marker.className = 'markers ' + value.class_name;
            marker.setAttribute('data-id', value['id']);
            marker.setAttribute("data-name", value['name']);
            marker.setAttribute('data-display', value['display']);

            var markerLabel = new CSS2DObject(marker);
            markerLabel.position.set(value['x'], value['y'], value['z']);

            Panorama.panel(value);

            if (value['category'] == 'home') {
                current_page = value['id'];
            }

            scene.add(markerLabel);
            // scene1.add(markerLabel);
            // scene2.add(markerLabel);

            if (value['display'] == 'hidden') {
                marker.classList.add('hidden');
            }

            marker.ontouchstart = function () {
                current_page = value['id'];
                Panorama.overlayChange(value);
            }

            marker.onclick = function () {
                current_page = value['id'];
                Panorama.overlayChange(value);
                // scene_transition = true;
                // time = 0;
                // scene_interval = setInterval(function () {
                //     time += 0.01;
                //     if (time > 1 && scene_transition) {
                //         time = 1;
                //         scene_transition = false;
                //         scene2.background = new THREE.CubeTextureLoader().load(value.cube_texture);
                //     }
                // }, 18);

                // Panorama.resetCameraPosition();
            };
        });
        // var texture = new THREE.TextureLoader().load('img/pic/disc.png');
        //
        // var material = new THREE.PointsMaterial({
        //     color: 0x48a6d5,
        //     size: 1,
        //     map: texture,
        //     depthWrite: false,
        //     transparent: true
        // });

        // for (var i = 0; i < 2; i++) {
        // geometry = new THREE.Geometry();
        // geometry.vertices.push(new THREE.Vector3(10, 3.9, z));
        // geometry.name = 'geometry' + i;
        //
        // points = new THREE.Points(geometry, material);

        // var earthDiv = document.createElement('div');
        // earthDiv.className = 'panel';
        // earthDiv.textContent = '資訊面板';
        // earthDiv.style.marginTop = '-50px';
        // var earthLabel = new CSS2DObject(earthDiv);
        // earthLabel.position.set(10, 3.9, z);
        // points.add(earthLabel);
        // scene.add(earthLabel);

        // var marker = document.createElement('img');
        // marker.className = 'marker';
        // marker.src = "img/pic/disc.png";
        // marker.setAttribute("data-id", i);

        // var marker = document.createElement('div');
        // marker.className = "marker-animate";
        // marker.setAttribute("data-id", i);
        //
        // var markerLabel = new CSS2DObject(marker);
        // markerLabel.position.set(10, 3.9, z);
        // scene.add(markerLabel);
        //
        // marker.onclick = function () {
        //     console.log(this.getAttribute('data-id'));
        // };

        // points_collect.push(points);

        //     z += 3;
        // }

        // 大量標點
        // geometry = new THREE.BufferGeometry();
        // var position = [
        //     10, 3.9, -0.3,
        //     10, 3.9, 2
        // ];
        // var size = [
        //     1
        // ];
        //
        // geometry.setAttribute('position', new THREE.Float32BufferAttribute(position, 3));
        // geometry.setAttribute('size', new THREE.Float32BufferAttribute(size, 1));
        //
        // var texture = new THREE.TextureLoader().load("img/pic/disc.png");
        //
        // var material = new THREE.PointsMaterial({
        //     color: 0x48a6d5,
        //     size: 1,
        //     map: texture,
        //     depthWrite: false,
        //     transparent: true
        // });
        //
        // points = new THREE.Points(geometry, material);
        // scene.add(points);
    },
    panel: function (value) {
        if ("panel" in value) {
            value['panel'].forEach(function (item) {
                var panel = document.createElement('div');
                panel.className = 'panel hidden';
                panel.setAttribute('data-id', value['id']);
                panel.innerHTML = '<div class="panel-title">' + item.name + '</div><div class="panel-content">' + item.content + '</div>';

                var panelElement = new CSS2DObject(panel);
                panelElement.position.set(item['x'], item['y'], item['z']);

                scene.add(panelElement);
            });
        }
    },
    overlayChange: function (value) {
        $('.overlay').addClass('change');

        setTimeout(function () {
            scene.background = new THREE.CubeTextureLoader().load(value.cube_texture);
        }, 300);

        setTimeout(function () {
            Panorama.resetCameraPosition();
            // Panorama.panel(value);
            $('.overlay').removeClass('change');
            var elements = document.getElementsByClassName('markers');
            for (var i = 0; i < elements.length; i++) {
                elements[i].classList.add('hidden');
                if (value['category'] == 'home' && elements[i].getAttribute('data-display') == 'show') {
                    elements[i].classList.remove('hidden');
                } else if ("sub_marker_ids" in value) { // 判斷此標籤底下有無子標籤
                    if (value['sub_marker_ids'].includes(parseInt(elements[i].getAttribute('data-id')))) {
                        elements[i].classList.remove('hidden');
                    }
                }
            }

            var panel_elements = document.getElementsByClassName('panel');
            for (var i = 0; i < panel_elements.length; i++) {
                panel_elements[i].classList.add('hidden');
                if ("panel" in value) {
                    if (parseInt(panel_elements[i].getAttribute('data-id')) == current_page) {
                        panel_elements[i].classList.remove('hidden');
                    }
                }
            }

        }, 1000);
    },
    labels: function () {
        dots = new THREE.Object3D();

        var texture = new THREE.TextureLoader().load("img/pic/disc.png");

        var circleGeometry = new THREE.SphereGeometry(0.35, 10);
        var material = new THREE.MeshBasicMaterial({
            color: 0x48a6d5,
            map: texture,
            depthWrite: false,
            transparent: true
        });
        var circle = new THREE.Mesh(circleGeometry, material);

        circle.position.x = 10;
        circle.position.y = 3.9;
        circle.position.z = 9;
        dots.add(circle);

        var earthDiv = document.createElement('div');
        earthDiv.className = 'panel';
        earthDiv.textContent = '資訊面板';
        earthDiv.style.marginTop = '-50px';
        var earthLabel = new CSS2DObject(earthDiv);
        earthLabel.position.set(10, 3.9, 9);
        dots.add(earthLabel);

        scene.add(dots);
    },
    onWindowResize: function () {
        camera.aspect = window.innerWidth / window.innerHeight;
        camera.updateProjectionMatrix();
        // camera2.aspect = window.innerWidth / window.innerHeight;
        // camera2.updateProjectionMatrix();
        renderer.setSize(window.innerWidth, window.innerHeight);
    },
    onDocumentMouseWheel: function (event) {
        // var fov = camera.fov + event.deltaY * 0.05;
        // camera.fov = THREE.Math.clamp(fov, 50, 80);
        // camera.updateProjectionMatrix();

        var fov = camera.fov + event.deltaY * 0.05;
        camera.fov = THREE.Math.clamp(fov, 50, 90);
        camera.updateProjectionMatrix();
    },
    onMouseDown: function (event) {
        $('body').css('cursor', 'grabbing');
        // isUserInteracting = true;
        //
        // var clientX = event.clientX || event.touches[0].clientX;
        // var clientY = event.clientY || event.touches[0].clientY;
        //
        // onMouseDownMouseX = clientX;
        // onMouseDownMouseY = clientY;
        //
        // onMouseDownLon = lon;
        // onMouseDownLat = lat;

        // var intersects = raycaster.intersectObjects(points_collect);
        // if (intersects.length > 0) {
        //     console.log(intersects[0]['object']['geometry']['name']);
        // }
    },
    onMouseMove: function (event) {
        // if (isUserInteracting === true) {
        //     var clientX = event.clientX || event.touches[0].clientX;
        //     var clientY = event.clientY || event.touches[0].clientY;
        //
        //     lon = (onMouseDownMouseX - clientX) * 0.1 + onMouseDownLon;
        //     lat = (clientY - onMouseDownMouseY) * 0.1 + onMouseDownLat;
        // }
        //
        // mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
        // mouse.y = -(event.clientY / window.innerHeight) * 2 + 1;

        // intersects = raycaster.intersectObjects(points_collect);
        // if (intersects.length > 0) {
        //     $('html, body').css('cursor', 'pointer');
        // } else {
        //     $('html, body').css('cursor', 'default');
        // }
    },
    onMouseUp: function () {
        $('body').css('cursor', 'grab');
        // isUserInteracting = false;
    },
    animate: function () {
        requestAnimationFrame(Panorama.animate);
        Panorama.render();
    },
    render: function () {
        // raycaster.setFromCamera(mouse, camera);

        // if (isUserInteracting === false) {
        //     lon += 0.03;
        // }

        // lat = Math.max(-85, Math.min(85, lat));
        // phi = THREE.Math.degToRad(90 - lat);
        // theta = THREE.Math.degToRad(lon);
        //
        // camera.target.x = 500 * Math.sin(phi) * Math.cos(theta);
        // camera.target.y = 500 * Math.cos(phi);
        // camera.target.z = 500 * Math.sin(phi) * Math.sin(theta);
        //
        // camera.lookAt(camera.target);

        controls.update();

        // renderer.clear();
        // renderer.render(scene1, camera);
        // renderer.render(scene2, camera);

        image = renderer.domElement.toDataURL("image/jpeg");
        // socket.emit('chatroom', dataURIToBlob(renderer.domElement.toDataURL("image/jpeg")));

        // transition.render();

        renderer.render(scene, camera);
        labelRenderer.render(scene, camera);
    },
    _ajax: function () {
        this.markers_collection = {
            success: 1,
            message: '回應成功',
            data: {
                markers: [
                    {
                        id: 1,
                        name: '故宮南院',
                        x: 10, y: 3.9, z: -0.3,
                        display: 'show',
                        category: 'sub',
                        class_name: 'marker',
                        cube_texture: [
                            'img/cube/marker/1.jpg',
                            'img/cube/marker/3.jpg',
                            'img/cube/marker/4.jpg',
                            'img/cube/marker/5.jpg',
                            'img/cube/marker/0.jpg',
                            'img/cube/marker/2.jpg'
                        ],
                        panel: [
                            {
                                id: 1,
                                name: '故宮南院介紹',
                                x: 100, y: 3.9, z: -0.3,
                                content: '<p>國立故宮南院簡介 國立故宮南院位於嘉義縣太保市，並定位為「亞洲藝術文化博物館」，期盼與台北故宮博物院達到「平衡南北，文化均富」的理念，並帶動中南部地區的文化風氣與經濟發展。 故宮南院的園區幅員廣闊，除了有人工濕地、熱帶花園、水岸舞台等休閒區域，並設有至善湖、至德湖兩座人工湖。</p>'
                            }
                        ],
                        sub_marker_ids: [
                            6, 7, 8
                        ],
                        sub_marker_position: [
                            {x: 10, y: 3.9, z: -0.3},
                            {x: 10, y: 3.9, z: -0.3},
                            {x: 10, y: 3.9, z: -0.3}
                        ]
                    },
                    {
                        id: 2,
                        name: '高跟鞋教堂',
                        x: -2.5, y: 15, z: -40,
                        display: 'show',
                        category: 'sub',
                        class_name: 'marker',
                        cube_texture: [
                            'img/cube/marker1/1.jpg',
                            'img/cube/marker1/3.jpg',
                            'img/cube/marker1/4.jpg',
                            'img/cube/marker1/5.jpg',
                            'img/cube/marker1/0.jpg',
                            'img/cube/marker1/2.jpg'
                        ]
                    },
                    {
                        id: 3,
                        name: '大三通飯店',
                        x: 10, y: 2.22, z: -1.55,
                        display: 'show',
                        category: 'sub',
                        class_name: 'dot',
                        cube_texture: [
                            'img/cube/itour1/1.jpg',
                            'img/cube/itour1/3.jpg',
                            'img/cube/itour1/4.jpg',
                            'img/cube/itour1/5.jpg',
                            'img/cube/itour1/0.jpg',
                            'img/cube/itour1/2.jpg'
                        ]
                    },
                    {
                        id: 4,
                        name: '安蘭居國際青年館',
                        x: 10, y: 2.3, z: -2.4,
                        display: 'show',
                        category: 'sub',
                        class_name: 'dot',
                        cube_texture: [
                            'img/cube/itour2/1.jpg',
                            'img/cube/itour2/3.jpg',
                            'img/cube/itour2/4.jpg',
                            'img/cube/itour2/5.jpg',
                            'img/cube/itour2/0.jpg',
                            'img/cube/itour2/2.jpg'
                        ]
                    },
                    {
                        id: 5,
                        name: '滿棠茶品',
                        x: 10, y: 2.4, z: -3.9,
                        display: 'show',
                        category: 'sub',
                        class_name: 'dot',
                        cube_texture: [
                            'img/cube/itour3/1.jpg',
                            'img/cube/itour3/3.jpg',
                            'img/cube/itour3/4.jpg',
                            'img/cube/itour3/5.jpg',
                            'img/cube/itour3/0.jpg',
                            'img/cube/itour3/2.jpg'
                        ]
                    },
                    {
                        id: 6,
                        name: '圓環',
                        x: -13, y: 5.4, z: 10,
                        display: 'hidden',
                        category: 'home',
                        class_name: 'marker',
                        cube_texture: [
                            'img/cube/itour/1.jpg',
                            'img/cube/itour/3.jpg',
                            'img/cube/itour/4.jpg',
                            'img/cube/itour/5.jpg',
                            'img/cube/itour/0.jpg',
                            'img/cube/itour/2.jpg'
                        ]
                    },
                    {
                        id: 7,
                        name: '朴子黑白切',
                        x: 6.25, y: 1.9, z: -6.9,
                        display: 'hidden',
                        category: 'sub',
                        class_name: 'dot',
                        cube_texture: [
                            'img/cube/itour4/1.jpg',
                            'img/cube/itour4/3.jpg',
                            'img/cube/itour4/4.jpg',
                            'img/cube/itour4/5.jpg',
                            'img/cube/itour4/0.jpg',
                            'img/cube/itour4/2.jpg'
                        ]
                    },
                    {
                        id: 8,
                        name: '新寶珍餅店',
                        x: 10, y: 2.5, z: 5.78,
                        display: 'hidden',
                        category: 'sub',
                        class_name: 'dot',
                        cube_texture: [
                            'img/cube/itour5/1.jpg',
                            'img/cube/itour5/3.jpg',
                            'img/cube/itour5/4.jpg',
                            'img/cube/itour5/5.jpg',
                            'img/cube/itour5/0.jpg',
                            'img/cube/itour5/2.jpg'
                        ]
                    },
                ]
            }
        };
    }
};

function FXScene(background) {
    this.camera = new THREE.PerspectiveCamera(90, window.innerWidth / window.innerHeight, 0.1, 2000);
    this.camera.position.z = 0.01;
    this.scene = new THREE.Scene();
    this.scene.background = background;
    this.background = '';
    var renderTargetParameters = {
        minFilter: THREE.LinearFilter,
        magFilter: THREE.LinearFilter,
        format: THREE.RGBFormat,
        stencilBuffer: true
    };
    this.add = function (object) {
        this.scene.add(object);
    };
    this.controls = new OrbitControls(this.camera, document.querySelector('#container'));
    this.controls.enablePan = false;
    this.controls.enableZoom = false;
    this.controls.enableDamping = true;
    this.controls.rotateSpeed = -0.25;
    this.controls.autoRotate = true;
    this.controls.autoRotateSpeed = 0.25;
    this.fbo = new THREE.WebGLRenderTarget(window.innerWidth, window.innerHeight, renderTargetParameters);
    this.render = function () {
        if (this.background) {
            this.scene.background = this.background;
        }
        this.controls.update();
        renderer.setRenderTarget(this.fbo);
        renderer.clear();
        renderer.render(this.scene, this.camera);
        labelRenderer.render(this.scene, this.camera);
    };
}

function Transition(sceneA, sceneB) {
    this.scene = new THREE.Scene();
    this.cameraOrtho = new THREE.OrthographicCamera(window.innerWidth / -2, window.innerWidth / 2, window.innerHeight / 2, window.innerHeight / -2, -10, 10);
    this.texture = new THREE.TextureLoader().load('img/transition/transition1.png');
    this.quadmaterial = new THREE.ShaderMaterial({
        uniforms: {
            tDiffuse1: {value: null},
            tDiffuse2: {value: null},
            mixRatio: {value: 0.0},
            threshold: {value: 0.1},
            useTexture: {value: transitionParams.useTexture},
            tMixTexture: {value: this.texture}
        },
        vertexShader: [
            "varying vec2 vUv;",
            "void main() {",
            "vUv = vec2( uv.x, uv.y );",
            "gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );",
            "}"
        ].join("\n"),
        fragmentShader: [
            "uniform float mixRatio;",
            "uniform sampler2D tDiffuse1;",
            "uniform sampler2D tDiffuse2;",
            "uniform sampler2D tMixTexture;",
            "uniform int useTexture;",
            "uniform float threshold;",
            "varying vec2 vUv;",
            "void main() {",
            "	vec4 texel1 = texture2D( tDiffuse1, vUv );",
            "	vec4 texel2 = texture2D( tDiffuse2, vUv );",
            "	if (useTexture==1) {",
            "		vec4 transitionTexel = texture2D( tMixTexture, vUv );",
            "		float r = mixRatio * (1.0 + threshold * 2.0) - threshold;",
            "		float mixf=clamp((transitionTexel.r - r)*(1.0/threshold), 0.0, 1.0);",
            "		gl_FragColor = mix( texel1, texel2, mixf );",
            "	} else {",
            "		gl_FragColor = mix( texel2, texel1, mixRatio );",
            "	}",
            "}"
        ].join("\n")
    });
    var quadgeometry = new THREE.PlaneBufferGeometry(window.innerWidth, window.innerHeight);
    this.quad = new THREE.Mesh(quadgeometry, this.quadmaterial);
    this.scene.add(this.quad);
    this.sceneA = sceneA;
    this.sceneB = sceneB;
    this.quadmaterial.uniforms.tDiffuse1.value = sceneA.fbo.texture;
    this.quadmaterial.uniforms.tDiffuse2.value = sceneB.fbo.texture;
    this.render = function () {
        // var t = (1 + Math.sin(transitionParams.transitionSpeed * clock.getElapsedTime() / Math.PI)) / 2;
        // transitionParams.transition = t;
        transitionParams.transition = time;

        if (time == 1) {
            clearInterval(scene_interval);
        }
        // transitionParams.transition = THREE.Math.smoothstep(time, 0.3, 0.7);
        // console.log(transitionParams.transition);
        this.quadmaterial.uniforms.tMixTexture.value = this.texture;
        this.quadmaterial.uniforms.mixRatio.value = transitionParams.transition;

        this.sceneA.render();
        this.sceneB.render();
        renderer.setRenderTarget(null);
        renderer.clear();
        renderer.render(this.scene, this.cameraOrtho);
    };
}

Panorama.init();
Panorama.animate();

// 全螢幕
ScreenFull.init('btn_screen', 'img/controls/btn_enter_fs.png', 'img/controls/btn_close_fs.png');

document.getElementById('btn_home').ontouchstart = function () {
    Panorama.markers_collection.data.markers.forEach(function (value) {
        if (value['category'] == 'home') {
            current_page = value['id'];
            Panorama.overlayChange(value);
        }
    });
};

// 回首頁
$('#btn_home').on('click', function () {
    Panorama.markers_collection.data.markers.forEach(function (value) {
        if (value['category'] == 'home') {
            current_page = value['id'];
            Panorama.overlayChange(value);
            // scene.background = new THREE.CubeTextureLoader().load(value.cube_texture);

            // time = 0;
            // scene_interval = setInterval(function () {
            //     time += 0.01;
            //     if (time > 1 && scene_transition) {
            //         time = 1;
            //         scene_transition = false;
            //         scene2.background = new THREE.CubeTextureLoader().load(value.cube_texture);
            //     }
            // }, 18);

            // var elements = document.getElementsByClassName('markers');
            // for (var i = 0; i < elements.length; i++) {
            //     elements[i].classList.add('hidden');
            //     if (value['category'] == 'home' && elements[i].getAttribute('data-display') == 'show') {
            //         elements[i].classList.remove('hidden');
            //     }
            // }
            // Panorama.resetCameraPosition();
        }
    });
});