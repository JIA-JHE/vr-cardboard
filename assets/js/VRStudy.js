import * as THREE from '../plugins/threejs/build/three.module.js';
import {CSS2DObject, CSS2DRenderer} from "../plugins/threejs/jsm/renderers/CSS2DRenderer.js";
import {OrbitControls} from "../plugins/threejs/jsm/controls/OrbitControls.js";
import {LightProbeGenerator} from "../plugins/threejs/jsm/lights/LightProbeGenerator.js";
import {NodeMaterialLoader, NodeMaterialLoaderUtils} from '../plugins/threejs/jsm/loaders/NodeMaterialLoader.js';
import {
    NodeFrame,
    SpriteNodeMaterial,
    MathNode,
    OperatorNode,
    TextureNode,
    Vector2Node,
    TimerNode,
    FunctionNode,
    FunctionCallNode,
    PositionNode,
    SwitchNode,
    UVNode
} from '../plugins/threejs/jsm/nodes/Nodes.js';

'use strict';

var container, stats;
var camera, scene, renderer, labelRenderer, mesh;
var current_page;

var lon = 0, lat = 0;

var raycaster, mouse, mouse3, controls;
var transition_texture, markerURL, markerTexture, sprite1, group, plane_group;
var library = {};
var text_loader;

var frame = new NodeFrame();
var clock = new THREE.Clock();
var monitor, directionalLight, test_group;

var API = {
    lightProbeIntensity: 1.0,
    directionalLightIntensity: 0.2,
    envMapIntensity: 1
};

var NormalGame = {
    markers_collection: null,
    init: function () {
        this._ajax();
        raycaster = new THREE.Raycaster();
        mouse = new THREE.Vector2();
        mouse3 = new THREE.Vector3();
        text_loader = new THREE.FontLoader();

        container = document.getElementById('normal-scene');

        stats = new Stats();
        container.appendChild(stats.dom);

        camera = new THREE.PerspectiveCamera(90, window.innerWidth / window.innerHeight, 0.1, 1000);
        camera.position.set(0, 0, -0.01);

        scene = new THREE.Scene();

        var geometry = new THREE.SphereBufferGeometry(500, 60, 40);
        geometry.scale(-1, 1, 1);

        var texture = new THREE.TextureLoader().load('img/vr-study/panorama/1.png');
        var material = new THREE.MeshBasicMaterial({map: texture});

        mesh = new THREE.Mesh(geometry, material);

        scene.add(mesh);

        directionalLight = new THREE.DirectionalLight(0xffffff, API.directionalLightIntensity);
        directionalLight.position.set(10, 10, 10);
        scene.add(directionalLight);

        var helper = new THREE.GridHelper(1000, 40, 0x303030, 0x303030);
        helper.position.y = -75;
        // scene.add(helper);

        NormalGame.audio();
        NormalGame.points();

        // text_loader.load('assets/fonts/HanWangHeiLight_Regular.json', function (res) {
        //     NormalGame.text(res);
        // });

        renderer = new THREE.WebGLRenderer({antialias: true});
        renderer.setPixelRatio(window.devicePixelRatio);
        renderer.setSize(window.innerWidth, window.innerHeight);
        container.appendChild(renderer.domElement);

        labelRenderer = new CSS2DRenderer();
        labelRenderer.setSize(window.innerWidth, window.innerHeight);
        labelRenderer.domElement.style.position = 'absolute';
        labelRenderer.domElement.style.top = 0;
        container.appendChild(labelRenderer.domElement);

        controls = new OrbitControls(camera, document.querySelector('#normal-scene'));
        controls.enablePan = false;
        controls.enableZoom = false;
        controls.enableDamping = true;
        controls.zoomSpeed = 1.2;
        controls.rotateSpeed = -0.25;
        controls.autoRotate = false;
        controls.autoRotateSpeed = 0.25;

        // controls.enableDamping = true; // an animation loop is required when either damping or auto-rotation are enabled
        // controls.dampingFactor = 0.05;
        // controls.screenSpacePanning = false;
        // controls.minDistance = 100;
        // controls.maxDistance = 500;
        // controls.maxPolarAngle = Math.PI / 2;

        document.addEventListener('touchstart', NormalGame.onTouchDown, false);
        document.addEventListener('touchend', NormalGame.onTouchUp, false);
        document.addEventListener('touchmove', NormalGame.onMouseMove, false);

        document.addEventListener('mousedown', NormalGame.onMouseDown, false);
        document.addEventListener('mousemove', NormalGame.onMouseMove, false);
        document.addEventListener('mouseup', NormalGame.onMouseUp, false);
        document.addEventListener('wheel', NormalGame.onDocumentMouseWheel, false);
        window.addEventListener('resize', NormalGame.onWindowResize, false);
    },
    resetCameraPosition: function () {
        lon = 0;
        lat = 0;
        camera.position.set(0, 0, 0.01);
    },
    // 音訊
    audioListener: null,
    audioSound: null,
    audio: function () {
        NormalGame.audioListener = new THREE.AudioListener();
        camera.add(NormalGame.audioListener);

        NormalGame.audioSound = new THREE.Audio(NormalGame.audioListener);

        var audioLoader = new THREE.AudioLoader();
        audioLoader.load('assets/sounds/song.mp3', function (buffer) {
            NormalGame.audioSound.setBuffer(buffer);
            NormalGame.audioSound.setLoop(true);
            NormalGame.audioSound.setVolume(0.5);
            // NormalGame.audioSound.play();
        });
    },
    // 標點
    points: function () {
        group = new THREE.Group();
        scene.add(group);

        var url = "img/vr-study/components/UI_07_Button_question-mark.png";
        var texture = new THREE.TextureLoader().load(url);
        var material;
        var sprite;
        this.markers_collection.data.markers.forEach(function (value) {
            material = new THREE.SpriteMaterial({map: texture, color: 0xffffff, fog: true});
            sprite = new THREE.Sprite(material);
            sprite.scale.set(10, 10, 10);
            sprite.position.set(value['x'], value['y'], value['z']);
            sprite.data_id = value['id'];
            sprite.name = value['name'];

            text_loader.load('assets/fonts/HanWangHeiLight_Regular.json', function (res) {
                plane_group = new THREE.Group();
                plane_group.name = '一般圖片題';
                plane_group.data_id = value['id'];
                scene.add(plane_group);

                NormalGame.text(res, value);
                NormalGame.panels(value);
                plane_group.visible = false;
            });

            if (value['display'] == true) {
                sprite.visible = true;
            } else {
                sprite.visible = false;
            }
            group.add(sprite);
        });

        test_group = new THREE.Group();
        var geometry = new THREE.PlaneGeometry(30, 20, 0);
        var plane_texture = new THREE.TextureLoader().load('img/vr-study/components/UI_010_Sorting-topicBG.png');
        var material = new THREE.MeshBasicMaterial({
            map: plane_texture,
            transparent: true
        });
        var plane = new THREE.Mesh(geometry, material);
        plane.position.set(0, 0, 0);
        plane.renderOrder = 0;
        plane.material.depthTest = false;
        test_group.add(plane);

        var geometry = new THREE.PlaneGeometry(22, 14.2, 0);
        var plane_texture = new THREE.TextureLoader().load('img/vr-study/components/VRBGWindow.png');
        var material = new THREE.MeshBasicMaterial({
            map: plane_texture,
            transparent: true
        });
        var plane = new THREE.Mesh(geometry, material);
        plane.position.set(10, 0, 0);
        plane.renderOrder = 1;
        plane.material.depthTest = false;
        test_group.add(plane);

        test_group.position.set(0, 0, 10);

        scene.add(test_group);

        // var test = [];
        // test['x'] = 31;
        // test['y'] = 5;
        // test['z'] = 10;
        // test['id'] = 1;
        // NormalGame.panels(test);

        // sprite = new THREE.Sprite(material);
        // sprite.scale.set(10, 10, 10);
        // sprite.position.set(value['x'], value['y'], value['z']);
        // sprite.data_id = value['id'];
        // sprite.name = value['name'];
        //
        // if (value['display'] == true) {
        //     sprite.visible = true;
        // } else {
        //     sprite.visible = false;
        // }
        // group.add(sprite);

        // markerURL = "img/marker/marker2.png";
        // markerTexture = new THREE.TextureLoader().load(markerURL);
        // markerTexture.wrapS = markerTexture.wrapT = THREE.RepeatWrapping;
        // library[markerURL] = markerTexture;

        // scene.add(sprite1 = new THREE.Sprite(new SpriteNodeMaterial()));
        // sprite1.scale.x = 10;
        // sprite1.scale.y = 10;
        // sprite1.position.set(60, 20, 50);
        // sprite1.material.color = new TextureNode(markerTexture);
        // sprite1.material.color.uv = createHorizontalSpriteSheetNode(10, 7);
        // sprite1.material.blending = THREE.CustomBlending;
        // sprite1.material.blendSrc = THREE.OneFactor;
        // sprite1.material.blendDst = THREE.OneMinusSrcAlphaFactor;
        // sprite1.material.alpha = new SwitchNode(new TextureNode(markerTexture, sprite1.material.color.uv));
        // console.log(sprite1.material.alpha);
        // spriteToJSON(sprite1);
        //
        // function spriteToJSON(sprite) {
        //     var json = sprite.material.toJSON();
        //     NodeMaterialLoaderUtils.replaceUUID(json, markerTexture, markerURL);
        //     var material = new NodeMaterialLoader(null, library).parse(json);
        //     sprite.material.dispose();
        //     sprite.material = material;
        //     sprite.transparent = true;
        // }
        //
        // function createHorizontalSpriteSheetNode(hCount, speed) {
        //     var speed = new Vector2Node(0, speed); // frame per second
        //     var scale = new Vector2Node(1, 1 / hCount); // 8 horizontal images in sprite-sheet
        //     var uvTimer = new OperatorNode(
        //         new TimerNode(),
        //         speed,
        //         OperatorNode.MUL
        //     );
        //     var uvIntegerTimer = new MathNode(
        //         uvTimer,
        //         MathNode.FLOOR
        //     );
        //     var uvFrameOffset = new OperatorNode(
        //         uvIntegerTimer,
        //         scale,
        //         OperatorNode.MUL
        //     );

        //     var uvScale = new OperatorNode(
        //         new UVNode(),
        //         scale,
        //         OperatorNode.MUL
        //     );
        //     var uvFrame = new OperatorNode(
        //         uvScale,
        //         uvFrameOffset,
        //         OperatorNode.ADD
        //     );
        //     return uvFrame;
        // }

        // this.markers_collection.data.markers.forEach(function (value) {
        //     var marker = document.createElement('div');
        //     marker.className = 'markers ' + value.class_name;
        //     marker.setAttribute('data-id', value['id']);
        //     marker.setAttribute("data-name", value['name']);
        //     marker.setAttribute('data-display', value['display']);
        //
        //     var markerLabel = new CSS2DObject(marker);
        //     markerLabel.position.set(value['x'], value['y'], value['z']);
        //
        //     NormalGame.panel(value);
        //
        //     if (value['category'] == 'home') {
        //         current_page = value['id'];
        //     }
        //
        //     scene.add(markerLabel);
        //
        //     if (value['display'] == 'hidden') {
        //         marker.classList.add('hidden');
        //     }
        //
        //     marker.onclick = function () {
        //         current_page = value['id'];
        //         NormalGame.overlayChange(value);
        //     };
        // });
    },
    panelsCollection: [],
    panels: function (value) {
        var loader = new THREE.TextureLoader();

        var rotateX = 0;
        var rotateY = 180;
        var rotateZ = 0;
        var size = 0.05;

        // 面板
        var geometry = new THREE.PlaneGeometry(1458 * size, 1066 * size, 0);
        var plane_texture = loader.load('img/vr-study/components/UI_013_Text-window.png');
        var material = new THREE.MeshBasicMaterial({
            map: plane_texture,
            transparent: true
        });
        var plane = new THREE.Mesh(geometry, material);
        plane.position.set(0, 0, 0);
        plane.renderOrder = 0;
        plane.material.depthTest = false;
        plane_group.add(plane);

        // 標題
        var title_geometry = new THREE.PlaneGeometry(360 * size, 150 * size, 0);
        var title_plane_texture = loader.load('img/vr-study/components/UI_08_WindowText.png');
        var title_material = new THREE.MeshBasicMaterial({
            map: title_plane_texture,
            transparent: true
        });
        var title_plane = new THREE.Mesh(title_geometry, title_material);
        title_plane.position.set(-449.79 * size, 478.987 * size, 0);
        title_plane.renderOrder = 1;
        title_plane.material.depthTest = false;
        plane_group.add(title_plane);

        // var url = "img/vr-study/components/UI_013_Button_close.png";
        // var texture = new THREE.TextureLoader().load(url);
        // var material = new THREE.SpriteMaterial({map: texture, color: 0xffffff, fog: true});
        // var sprite;
        // sprite = new THREE.Sprite(material);
        // sprite.scale.set(10, 10, 10);
        // sprite.position.set(23, 12, 17);
        // sprite.name = 'close';
        // plane_group.add(sprite);

        // 關閉
        var close_geometry = new THREE.PlaneGeometry(165 * size, 165 * size, 0);
        var close_plane_texture = loader.load('img/vr-study/components/UI_013_Button_close.png');
        var close_material = new THREE.MeshBasicMaterial({
            map: close_plane_texture,
            transparent: true
        });
        var close_plane = new THREE.Mesh(close_geometry, close_material);
        close_plane.position.set(735.984 * size, 452.226 * size, 0);
        close_plane.renderOrder = 1;
        close_plane.material.depthTest = false;
        close_plane.name = '關閉';
        close_plane.data_id = value['id'];
        plane_group.add(close_plane);

        // 答案送出
        var geometry = new THREE.PlaneGeometry(3, 3, 0);
        var plane_texture = loader.load('img/vr-study/components/UI_08_Button_Answer-sent.png');
        var material = new THREE.MeshBasicMaterial({
            map: plane_texture,
            transparent: true
        });
        var plane = new THREE.Mesh(geometry, material);
        plane.position.set(0, 0, 0);
        plane.scale.set(.9, .9, .9);
        plane.renderOrder = 1;
        plane.material.depthTest = false;
        plane.name = '答案送出';
        plane_group.add(plane);

        plane_group.position.set(value['x'], value['y'], value['z']);
        plane_group.rotateY(180);
        plane_group.lookAt(0, 0, 0);
        NormalGame.panelsCollection[value['id']] = plane_group;
        // var materials = [material, plane_background_material];
        // geometry.faces[0].materialIndex = 0;
        // geometry.faces[1].materialIndex = 0;
        //
        // for (var i = 1; i < geometry.faces.length / 2; i++) {
        //     geometry.faces[i * 2].materialIndex = 1;
        //     geometry.faces[i * 2 + 1].materialIndex = 1;
        // }

        // var mesh = new THREE.Mesh(geometry, new THREE.MultiMaterial(materials));
        // mesh.position.set(30, 5, 10);
        // mesh.rotateY(180.2);
        // scene.add(mesh);

        // if ("panel" in value) {
        //     value['panel'].forEach(function (item) {
        //         var panel = document.createElement('div');
        //         panel.className = 'panel hidden';
        //         panel.setAttribute('data-id', value['id']);
        //         panel.innerHTML = '<div class="panel-title">' + item.name + '</div><div class="panel-content">' + item.content + '</div>';
        //
        //         var panelElement = new CSS2DObject(panel);
        //         panelElement.position.set(item['x'], item['y'], item['z']);
        //
        //         scene.add(panelElement);
        //     });
        // }
    },
    text: function (font, value) {
        var xMid, text;
        var color = 0xffffff;
        var matLite = new THREE.MeshBasicMaterial({
            color: color,
            transparent: true,
            opacity: 1,
        });

        // 標題
        var message = "一般圖片題";
        var shapes = font.generateShapes(message, 0.45);
        var geometry = new THREE.ShapeBufferGeometry(shapes);
        geometry.computeBoundingBox();
        xMid = -0.5 * (geometry.boundingBox.max.x - geometry.boundingBox.min.x);
        geometry.translate(xMid, 0, 0);
        text = new THREE.Mesh(geometry, matLite);
        text.position.set(0, 0, 0);
        text.renderOrder = 2;
        text.material.depthTest = false;
        text.lookAt(0, 0, 0);
        plane_group.add(text);

        // 內容
        var message = "720 + 音檔 + 圖片 //選蘋果";
        var shapes = font.generateShapes(message, 0.5);
        var geometry = new THREE.ShapeBufferGeometry(shapes);
        geometry.computeBoundingBox();
        xMid = -0.5 * (geometry.boundingBox.max.x - geometry.boundingBox.min.x);
        geometry.translate(xMid, 0, 0);
        text = new THREE.Mesh(geometry, matLite);
        text.position.set(0, 0, 0);
        text.lookAt(0, 0, 0);
        plane_group.add(text);

        // var geometry = new THREE.TextGeometry('一般圖片題', {
        //     font: NormalGame.font,
        //     size: 0.7,
        //     height: 0.1,
        //     curveSegments: 12,
        //     bevelEnabled: true,
        //     bevelThickness: 0.1,
        //     bevelSize: 0.01,
        //     bevelSegments: 3
        // });
        //
        // geometry.computeBoundingBox();
        // geometry.computeVertexNormals();
        //
        // var material = new THREE.MeshBasicMaterial({color: 0xffffff});
        // var mesh = new THREE.Mesh(geometry, material);
        // mesh.position.set(29, 11.7, 1.5);
        // mesh.rotateY(180.2);
        // plane_group.add(mesh);
    },
    // 轉場
    overlayChange: function (value) {
        $('.overlay').addClass('change');

        setTimeout(function () {
            scene.background = new THREE.CubeTextureLoader().load(value.cube_texture);
        }, 500);

        setTimeout(function () {
            NormalGame.resetCameraPosition();
            $('.overlay').removeClass('change');
            var elements = document.getElementsByClassName('markers');
            for (var i = 0; i < elements.length; i++) {
                elements[i].classList.add('hidden');
                if (value['category'] == 'home' && elements[i].getAttribute('data-display') == 'show') {
                    elements[i].classList.remove('hidden');
                } else if ("sub_marker_ids" in value) { // 判斷此標籤底下有無子標籤
                    if (value['sub_marker_ids'].includes(parseInt(elements[i].getAttribute('data-id')))) {
                        elements[i].classList.remove('hidden');
                    }
                }
            }

            var panel_elements = document.getElementsByClassName('panel');
            for (var i = 0; i < panel_elements.length; i++) {
                panel_elements[i].classList.add('hidden');
                if ("panel" in value) {
                    if (parseInt(panel_elements[i].getAttribute('data-id')) == current_page) {
                        panel_elements[i].classList.remove('hidden');
                    }
                }
            }

        }, 600);
    },
    // 視窗大小變化調整
    onWindowResize: function () {
        camera.aspect = window.innerWidth / window.innerHeight;
        camera.updateProjectionMatrix();
        renderer.setSize(window.innerWidth, window.innerHeight);
    },
    // 滑鼠滾輪事件
    onDocumentMouseWheel: function (event) {
        var fov = camera.fov + event.deltaY * 0.05;
        camera.fov = THREE.Math.clamp(fov, 50, 90);
        camera.updateProjectionMatrix();
    },
    // 滑鼠點擊事件
    selectedObject: null,
    onTouchDown: function (event) {
        NormalGame.onDownEvent(event, event.touches[0].clientX, event.touches[0].clientY);
    },
    onMouseDown: function (event) {
        NormalGame.onDownEvent(event, event.layerX, event.layerY);
    },
    onDownEvent: function (event, x, y) {
        event.preventDefault();
        var intersects = NormalGame.getGroupIntersects(group, x, y);
        if (intersects.length > 0) {
            var res = intersects.filter(function (res) {
                return res && res.object;
            })[0];
            if (res && res.object) {
                this.selectedObject = res.object;
                plane_group.visible = true;
                group.visible = false;
                NormalGame.panelsCollection[res.object.data_id].visible = true;
            }
        }

        NormalGame.panelsCollection.forEach(function (item) {
            var plane_intersects = NormalGame.getGroupIntersects(item, x, y);
            if (plane_intersects.length > 0) {
                var res = plane_intersects.filter(function (res) {
                    return res && res.object;
                })[0];
                if (res && res.object) {
                    if (res.object.name == '關閉' || res.object.name == '答案送出') {
                        NormalGame.panelsCollection[res.object.data_id].visible = false;
                        group.visible = true;
                    }
                }
            }
        });
    },
    // 滑鼠移動事件
    onMouseMove: function (event) {
        event.preventDefault();
        NormalGame.onMoveEvent(event, event.layerX, event.layerY);
    },
    onMoveEvent: function (event, x, y) {
        if (this.selectedObject) {
        }

        var intersects = NormalGame.getGroupIntersects(group, x, y);
        if (intersects.length > 0) {
            var res = intersects.filter(function (res) {
                return res && res.object;
            })[0];
            if (res && res.object) {
                this.selectedObject = res.object;
            }
        }

        var plane_intersects = NormalGame.getGroupIntersects(plane_group, x, y);
        if (plane_intersects.length > 0) {
            var res = plane_intersects.filter(function (res) {
                return res && res.object;
            })[0];
            if (res && res.object) {
                this.selectedObject = res.object;
            }
        }
    },
    // 滑鼠點擊後放開事件
    onTouchUp: function (event) {
        NormalGame.onUpEvent(event, event.touches[0].clientX, event.touches[0].clientY);

    },
    onMouseUp: function (event) {
        NormalGame.onUpEvent(event, event.layerX, event.layerY);
    },
    onUpEvent: function (event, x, y) {
        var intersects = NormalGame.getGroupIntersects(group, x, y);
        if (intersects.length > 0) {
            var res = intersects.filter(function (res) {
                return res && res.object;
            })[0];
            if (res && res.object) {
            }
        }

        var plane_intersects = NormalGame.getGroupIntersects(plane_group, x, y);
        if (plane_intersects.length > 0) {
            var res = plane_intersects.filter(function (res) {
                return res && res.object;
            })[0];
            if (res && res.object) {

            }
        }
    },
    // 取得投射交叉的物件
    getGroupIntersects: function (_group, x, y) {
        x = (x / window.innerWidth) * 2 - 1;
        y = -(y / window.innerHeight) * 2 + 1;
        mouse3.set(x, y, 0.5);
        raycaster.setFromCamera(mouse3, camera);
        return raycaster.intersectObject(_group, true);
    },
    animate: function () {
        requestAnimationFrame(NormalGame.animate);
        NormalGame.render();
    },
    current_second: 0,
    deg: 0,
    render: function () {
        // raycaster.setFromCamera(mouse, camera);
        // raycaster.setFromCamera(mouse, camera);
        // frame.update(clock.getDelta()).updateNode(sprite1.material);

        // console.log(NormalGame.audioSound);

        // console.log(camera.rotation.y);

        test_group.rotateZ(0.1);

        controls.update();
        renderer.render(scene, camera);
        labelRenderer.render(scene, camera);
        stats.update();

        if (parseInt(clock.getElapsedTime()) > this.current_second) {
            this.current_second = parseInt(clock.getElapsedTime());
            monitor = renderer.domElement.toDataURL("image/jpeg");
        }
    },
    _ajax: function () {
        this.markers_collection = {
            success: 1,
            message: '回應成功',
            data: {
                markers: [
                    {
                        id: 1,
                        name: '故宮南院',
                        x: 10, y: 3.9, z: 50,
                        display: true,
                        panel: [
                            {
                                id: 1,
                                name: '故宮南院介紹',
                                x: 100, y: 3.9, z: -0.3,
                                content: '<p>國立故宮南院簡介 國立故宮南院位於嘉義縣太保市，並定位為「亞洲藝術文化博物館」，期盼與台北故宮博物院達到「平衡南北，文化均富」的理念，並帶動中南部地區的文化風氣與經濟發展。 故宮南院的園區幅員廣闊，除了有人工濕地、熱帶花園、水岸舞台等休閒區域，並設有至善湖、至德湖兩座人工湖。</p>'
                            }
                        ],
                    },
                    {
                        id: 2,
                        name: '高跟鞋教堂',
                        x: -2.5, y: 15, z: 50,
                        display: true,
                    },
                    {
                        id: 3,
                        name: '大三通飯店',
                        x: 30, y: 2.22, z: 70,
                        display: true,
                    },
                    {
                        id: 4,
                        name: '安蘭居國際青年館',
                        x: 40, y: 2.5, z: 50,
                        display: true,
                    },
                    {
                        id: 5,
                        name: '滿棠茶品',
                        x: -15, y: 2.4, z: 60,
                        display: true,
                    },
                    {
                        id: 6,
                        name: '圓環',
                        x: -25, y: 5.4, z: 50,
                        display: false,
                    },
                    {
                        id: 7,
                        name: '朴子黑白切',
                        x: 70, y: 1.9, z: 50,
                        display: false,
                    },
                    {
                        id: 8,
                        name: '新寶珍餅店',
                        x: 60, y: 2.5, z: -50,
                        display: true,
                    },
                ]
            }
        };
    }
};

$('.welcome .form .btn-switch').on('click', function () {
    if ($(this).attr('data-switch') === 'true') {
        $(this).find('img').attr('src', 'img/vr-study/components/UI_01_Button_ChangeNameNurber.png');
        $(this).attr('data-switch', 'false');
        $('.form-switch-1').hide();
        $('.form-switch-2').show();
    } else {
        $(this).find('img').attr('src', 'img/vr-study/components/UI_01_Button_ChangeGroup.png');
        $(this).attr('data-switch', 'true');
        $('.form-switch-1').show();
        $('.form-switch-2').hide();
    }
});

$('.welcome .form .btn-confirm').on('click', function () {
    $('.welcome').hide();
    $('.start').show();
    $('.description').hide();
    $('.input-code').hide();
    $('.game-select').hide();
});

$('.start .btn-previous').on('click', function () {
    $('.welcome').show();
    $('.start').hide();
    $('.description').hide();
    $('.input-code').hide();
    $('.game-select').hide();
});

$('.start .btn-description').on('click', function () {
    $('.welcome').hide();
    $('.start').hide();
    $('.description').show();
    $('.input-code').hide();
    $('.game-select').hide();
});

$('.start .btn-start').on('click', function () {
    $('.welcome').hide();
    $('.start').hide();
    $('.description').hide();
    $('.input-code').show();
    $('.game-select').hide();
});

$('.description .btn-previous').on('click', function () {
    $('.welcome').hide();
    $('.start').show();
    $('.description').hide();
    $('.input-code').hide();
    $('.game-select').hide();
});

$('.input-code .btn-previous').on('click', function () {
    $('.welcome').hide();
    $('.start').show();
    $('.description').hide();
    $('.input-code').hide();
    $('.game-select').hide();
});

$('.input-code .btn-confirm').on('click', function () {
    $('.welcome').hide();
    $('.start').hide();
    $('.description').hide();
    $('.input-code').hide();
    $('.game-select').show();
});

$('.game-select .btn-previous').on('click', function () {
    $('.welcome').hide();
    $('.start').hide();
    $('.description').hide();
    $('.input-code').show();
    $('.game-select').hide();
});

$('.game-select #go-normal').on('click', function () {
    $('.welcome').hide();
    $('.start').hide();
    $('.description').hide();
    $('.input-code').hide();
    $('.game-select').hide();
    $('#normal-scene').show();
    NormalGame.init();
    NormalGame.animate();
});

$('.game-select #go-vr').on('click', function () {

});

var page_index = parseInt($('.intro-carousel .carousel-item.active').attr('data-key'));
var page_number = $('.intro-carousel .carousel-item').length;
var page = page_index + 1;
var current;
var current_info;

$('.description .intro-previous').on('click', function () {
    if (page_index - 1 >= 0) {
        page -= 1;
        page_index -= 1;
        $('.nav-page .page').text(page);
        current = $('.intro-carousel .carousel-item.active');
        current.removeClass('active');
        current.prev().addClass('active');
        current_info = $('.info-carousel .carousel-item.active');
        current_info.removeClass('active');
        current_info.prev().addClass('active');
    }
});

$('.description .intro-next').on('click', function () {
    if (page_index + 1 < page_number) {
        page += 1;
        page_index += 1;
        $('.nav-page .page').text(page);
        current = $('.intro-carousel .carousel-item.active');
        current.removeClass('active');
        current.next().addClass('active');
        current_info = $('.info-carousel .carousel-item.active');
        current_info.removeClass('active');
        current_info.next().addClass('active');
    }
});

// 全螢幕
ScreenFull.init('btn_screen', 'img/controls/btn_enter_fs.png', 'img/controls/btn_close_fs.png');

// 回首頁
$('#btn_home').on('click', function () {
    NormalGame.markers_collection.data.markers.forEach(function (value) {
        if (value['category'] == 'home') {
            current_page = value['id'];
            NormalGame.overlayChange(value);
        }
    });
});

// var socket = io('https://info.ouorange.com:6001');
// socket.on('connect', function () {
//     socket.emit('chatroom', 'success');
// });
//
//
// var canvas, context;
// setInterval(function () {
//     if (typeof renderer !== 'undefined' && typeof monitor !== 'undefined') {
//         socket.emit('chatroom', dataURIToBlob(monitor));
//     }
// }, 300);
//
// function dataURIToBlob(dataURI) {
//     var byteString = atob(dataURI.split(',')[1]);
//     var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0]
//     var ab = new ArrayBuffer(byteString.length);
//     var ia = new Uint8Array(ab);
//     for (var i = 0; i < byteString.length; i++) {
//         ia[i] = byteString.charCodeAt(i);
//     }
//
//     var blob = new Blob([ab], {type: mimeString});
//     return blob;
// }